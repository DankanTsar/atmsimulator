using System.Media;
using ATM.Model;

namespace ATM_UI
{
    public partial class MainForm : Form
    {
        enum ATMState
        {
            WaitingForCard,
            WaitingForCardRemove,
            WaitingForReceiptRemove,
            PINPrompt,
            MainView,
            MoneyWithdrawView,
            QueryStatusView,
            PaymentView,
            CardConfiscated,
            RequestReceipt,
            MoneyCountingView,
            WaitingForCardRemoveWithdraw,
            WaitingForReceiptRemoveWithdraw,
            WaitingForMoneyRemove,
            InsufficientFundsView,
            NotEnoughCashView
        }

        private Account _account;
        private protected Card Card;
        private ATMState _atmState = ATMState.WaitingForCard;
        private Transaction? _transaction;
        private string _inputBoxData = string.Empty;
        private int _secondTimer = 10;
        private bool _isCardAuthorized = false;
        private bool _printReceipt;

        public MainForm()
        {
            InitializeComponent();
        }

        private void cardreaderImage_Click(object sender, EventArgs e)
        {
            var cardInsertForm = new CardInsert();

            var result = cardInsertForm.ShowDialog();

            if (result == DialogResult.OK)
            {
                Card = Program.Proxy.GetCard(pan: cardInsertForm.cardPan);
                _atmState = ATMState.PINPrompt;
                setPinPromptView();
            }
        }

        private void oneBtn_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.PINPrompt)
            {
                displayRow3label.Text += "*";
                _inputBoxData += "1";
            }
            else if (_atmState == ATMState.MoneyWithdrawView)
            {
                displayRow3label.Text += "1";
                _inputBoxData += "1";
            }
        }

        private void twoBtn_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.PINPrompt)
            {
                displayRow3label.Text += "*";
                _inputBoxData += "2";
            }
            else if (_atmState == ATMState.MoneyWithdrawView)
            {
                displayRow3label.Text += "2";
                _inputBoxData += "2";
            }
        }

        private void threeBtn_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.PINPrompt)
            {
                displayRow3label.Text += "*";
                _inputBoxData += "3";
            }
            else if (_atmState == ATMState.MoneyWithdrawView)
            {
                displayRow3label.Text += "3";
                _inputBoxData += "3";
            }
        }

        private void fourBtn_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.PINPrompt)
            {
                displayRow3label.Text += "*";
                _inputBoxData += "4";
            }
            else if (_atmState == ATMState.MoneyWithdrawView)
            {
                displayRow3label.Text += "4";
                _inputBoxData += "4";
            }
        }

        private void fiveBtn_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.PINPrompt)
            {
                displayRow3label.Text += "*";
                _inputBoxData += "5";
            }
            else if (_atmState == ATMState.MoneyWithdrawView)
            {
                displayRow3label.Text += "5";
                _inputBoxData += "5";
            }
        }

        private void sixBtn_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.PINPrompt)
            {
                displayRow3label.Text += "*";
                _inputBoxData += "6";
            }
            else if (_atmState == ATMState.MoneyWithdrawView)
            {
                displayRow3label.Text += "6";
                _inputBoxData += "6";
            }
        }

        private void sevenBtn_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.PINPrompt)
            {
                displayRow3label.Text += "*";
                _inputBoxData += "7";
            }
            else if (_atmState == ATMState.MoneyWithdrawView)
            {
                displayRow3label.Text += "7";
                _inputBoxData += "7";
            }
        }

        private void eightBtn_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.PINPrompt)
            {
                displayRow3label.Text += "*";
                _inputBoxData += "8";
            }
            else if (_atmState == ATMState.MoneyWithdrawView)
            {
                displayRow3label.Text += "8";
                _inputBoxData += "8";
            }
        }

        private void nineBtn_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.PINPrompt)
            {
                displayRow3label.Text += "*";
                _inputBoxData += "9";
            }
            else if (_atmState == ATMState.MoneyWithdrawView)
            {
                displayRow3label.Text += "9";
                _inputBoxData += "9";
            }
        }

        private void zeroBtn_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.PINPrompt)
            {
                displayRow3label.Text += "*";
                _inputBoxData += "0";
            }
            else if (_atmState == ATMState.MoneyWithdrawView)
            {
                displayRow3label.Text += "0";
                _inputBoxData += "0";
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.PINPrompt)
            {
                _atmState = ATMState.WaitingForCardRemove;

                displayRow1label.Text = "Заберите карту";
                _inputBoxData = string.Empty;
                timer1.Enabled = true;
                _secondTimer = 10;
            }
        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            if (_atmState is ATMState.PINPrompt or ATMState.MoneyWithdrawView)
            {
                displayRow3label.Text = string.Empty;
                _inputBoxData = string.Empty;
            }
        }

        private void enterBtn_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.PINPrompt)
            {
                _isCardAuthorized = Program.Proxy.AuthorizeCard(Card, _inputBoxData);

                _inputBoxData = string.Empty;

                if (_isCardAuthorized)
                {
                    _atmState = ATMState.MainView;
                    setMainView();
                }
                else
                {
                    Card = Program.Proxy
                        .GetCard(pan: Card.Pan); // Получаем карту заново что бы понимать, что с ней дальше делать
                    if (Card.CardStatus == CardStatus.Unlocked)
                    {
                        displayRow2label.Text = $"Неверный пароль! Осталось {Card.PinAttempts} попыток";
                        displayRow3label.Text = string.Empty;
                        _inputBoxData = string.Empty;
                    }
                    else
                    {
                        _atmState = ATMState.CardConfiscated;
                        displayRow1label.Text = "Всё. Капут. Карта конфискована.";
                        displayRow2label.Text = "Обратитесь в банк, вам помогут.";
                        displayRow3label.Text = "Может быть помогут...";
                        displayRow4label.Text = string.Empty;
                        _inputBoxData = string.Empty;
                        Program.Proxy.ConfiscateCard(Card.Pan, Program.ATM.Id);

                        timer1.Enabled = true;
                        _secondTimer = 10;
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // throw new NotImplementedException("The method or operation is not implemented");
        }

        private void leftSideSecondBtn_Click(object sender, EventArgs e)
        {
            // throw new NotImplementedException("The method or operation is not implemented");
        }

        private void leftSideThirdBtn_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.MainView)
            {
                _atmState = ATMState.MoneyWithdrawView;
                HideSideMenu();
                ClearScreen();
                setMoneyWithdrawView();
            }
        }

        private void leftSideFourthBtn_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.MainView)
            {
                _atmState = ATMState.QueryStatusView;
                HideSideMenu();
                setQueryStatusView();
            }
            else if (_atmState == ATMState.QueryStatusView)
            {
                _atmState = ATMState.MainView;
                HideSideMenu();
                ClearScreen();
                setMainView();
            }
            else if (_atmState == ATMState.RequestReceipt) // Нет, не печатать
            {
                _atmState = ATMState.WaitingForCardRemoveWithdraw;
                HideSideMenu();

                displayRow1label.Text = "Заберите карту";
                _inputBoxData = string.Empty;
                timer1.Enabled = true;
                _secondTimer = 10;
                _printReceipt = false;
            }
        }

        private void rightSideFirstBtn_Click(object sender, EventArgs e)
        {
            // throw new NotImplementedException("The method or operation is not implemented");
        }

        private void rightSideSecondBtn_Click(object sender, EventArgs e)
        {
            // throw new NotImplementedException("The method or operation is not implemented");
        }

        private void rightSideThirdBtn_Click(object sender, EventArgs e)
        {
            // throw new NotImplementedException("The method or operation is not implemented");
        }

        private void rightSideFourthBtn_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.QueryStatusView)
            {
                _atmState = ATMState.WaitingForReceiptRemove;
                receiptPrinterImage.Image = global::ATM_UI.Properties.Resources.receipt_printer_printed;
                SoundPlayer simpleSound = new SoundPlayer(global::ATM_UI.Properties.Resources.receipt_print_sound);
                simpleSound.Play();
            }
            else if (_atmState == ATMState.MoneyWithdrawView)
            {
                if (string.IsNullOrWhiteSpace(_inputBoxData))
                    return;

                _atmState = ATMState.RequestReceipt;
                ClearScreen();
                HideSideMenu();
                setRequestReceiptView();
            }
            else if (_atmState == ATMState.RequestReceipt) // Да, печатать
            {
                _atmState = ATMState.WaitingForCardRemoveWithdraw;
                HideSideMenu();

                displayRow1label.Text = "Заберите карту";
                timer1.Enabled = true;
                _secondTimer = 10;
                _printReceipt = true;
            }
        }

        private void setPinPromptView()
        {
            if (_atmState == ATMState.PINPrompt)
            {
                displayRow1label.Text = "Введите PIN";
            }
        }

        private void setMainView()
        {
            if (_atmState == ATMState.MainView)
            {
                ClearScreen();

                sideMenuLeft3Label.Visible = true;
                sideMenuLeft3Label.Text = "Снять наличные";
                sideMenuLeft4Label.Visible = true;
                sideMenuLeft4Label.Text = "Узнать остаток";
                sideMenuRight4Label.Visible = true;
                sideMenuRight4Label.Text = "Произвести платёж";
            }
        }

        private void setQueryStatusView()
        {
            sideMenuLeft4Label.Visible = true;
            sideMenuLeft4Label.Text = "Назад";
            sideMenuRight4Label.Visible = true;
            sideMenuRight4Label.Text = "Распечатать";

            _account = Program.Proxy.GetAccount(Card.Pan);
            displayRow1label.Text = "Баланс счёта:";
            displayRow2label.Text = $@"{_account.VisibleMoney} $";
        }

        private void setMoneyWithdrawView()
        {
            displayRow1label.Text = "Снятие наличных";
            displayRow2label.Text = "Введите сумму";

            sideMenuLeft4Label.Visible = true;
            sideMenuLeft4Label.Text = "Назад";
            sideMenuRight4Label.Visible = true;
            sideMenuRight4Label.Text = "Продолжить";
        }

        private void setRequestReceiptView()
        {
            displayRow1label.Text = "Печатать чек?";

            sideMenuLeft4Label.Visible = true;
            sideMenuLeft4Label.Text = "Нет";
            sideMenuRight4Label.Visible = true;
            sideMenuRight4Label.Text = "Да";
        }

        private void setMoneyCountingView()
        {
            displayRow1label.Text = "Ожидайте";

            int amount;

            if (!int.TryParse(_inputBoxData, out amount))
            {
                return;
            }

            _transaction =
                Program.Proxy.WithdrawMoney(Program.ATM.Id, Card.Pan, int.Parse(_inputBoxData), _printReceipt);

            if (_transaction.Status == TransactionStatus.Completed)
            {
                SoundPlayer simpleSound = new SoundPlayer(global::ATM_UI.Properties.Resources.money_counter_sound);
                simpleSound.Play();

                _atmState = ATMState.WaitingForMoneyRemove;
                ClearScreen();
                HideSideMenu();

                displayRow1label.Text = "Заберите деньги!";

                moneyDispencerImage.Image = global::ATM_UI.Properties.Resources.money_dispencer_filled;
            }
            else if (_transaction.Status == TransactionStatus.InsufficientAccountFunds)
            {
                displayRow1label.Text = "Недостаточно денег на счёте!";

                timer1.Enabled = true;
                _secondTimer = 5;
                _atmState = ATMState.InsufficientFundsView;
            }
            else if (_transaction.Status == TransactionStatus.InsufficientAtmMoney)
            {
                displayRow1label.Text = "Банкомат не содержит достаточного кол-ва купюр!";
                displayRow2label.Text = "Состояние счёта не изменилось!";
                displayRow2label.Text = "Попробуйте позже";

                timer1.Enabled = true;
                _secondTimer = 5;
                _atmState = ATMState.NotEnoughCashView;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (_atmState == ATMState.WaitingForCardRemove)
            {
                if (_secondTimer > 0)
                {
                    _secondTimer--;
                    displayRow3label.Text = _secondTimer.ToString();
                }
                else
                {
                    timer1.Enabled = false;
                    displayRow1label.Text = "Вставьте карту";
                    displayRow3label.Text = string.Empty;
                    _atmState = ATMState.WaitingForCard;
                }
            }

            if (_atmState == ATMState.CardConfiscated)
            {
                if (_secondTimer > 0)
                {
                    _secondTimer--;
                    displayRow4label.Text = _secondTimer.ToString();
                }
                else
                {
                    timer1.Enabled = false;
                    displayRow1label.Text = "Вставьте карту";
                    displayRow2label.Text = string.Empty;
                    displayRow3label.Text = string.Empty;
                    displayRow4label.Text = string.Empty;
                    _atmState = ATMState.WaitingForCard;
                }
            }

            if (_atmState == ATMState.WaitingForCardRemoveWithdraw)
            {
                if (_secondTimer > 0)
                {
                    _secondTimer--;
                    displayRow4label.Text = _secondTimer.ToString();
                }
                else
                {
                    timer1.Enabled = false;

                    ClearScreen();
                    HideSideMenu();

                    _atmState = ATMState.MoneyCountingView;
                    setMoneyCountingView();
                }
            }

            if (_atmState is ATMState.InsufficientFundsView or ATMState.NotEnoughCashView)
            {
                if (_secondTimer > 0)
                {
                    _secondTimer--;
                    displayRow4label.Text = _secondTimer.ToString();
                }
                else
                {
                    timer1.Enabled = false;
                    displayRow1label.Text = "Вставьте карту";
                    displayRow2label.Text = string.Empty;
                    displayRow3label.Text = string.Empty;
                    displayRow4label.Text = string.Empty;
                    _atmState = ATMState.WaitingForCard;
                }
            }
        }

        private void EndSession()
        {
            ClearScreen();
            HideSideMenu();

            _inputBoxData = string.Empty;
            _atmState = ATMState.WaitingForCard;
            displayRow1label.Text = "Вставьте карту";
        }

        public void ClearScreen()
        {
            displayRow1label.Text = string.Empty;
            displayRow2label.Text = string.Empty;
            displayRow3label.Text = string.Empty;
            displayRow4label.Text = string.Empty;

            sideMenuLeft1Label.Text = string.Empty;
            sideMenuLeft2Label.Text = string.Empty;
            sideMenuLeft3Label.Text = string.Empty;
            sideMenuLeft4Label.Text = string.Empty;
            sideMenuRight1Label.Text = string.Empty;
            sideMenuRight2Label.Text = string.Empty;
            sideMenuRight3Label.Text = string.Empty;
            sideMenuRight4Label.Text = string.Empty;
        }

        public void HideSideMenu()
        {
            sideMenuLeft1Label.Visible = false;
            sideMenuLeft2Label.Visible = false;
            sideMenuLeft3Label.Visible = false;
            sideMenuLeft4Label.Visible = false;
            sideMenuRight1Label.Visible = false;
            sideMenuRight2Label.Visible = false;
            sideMenuRight3Label.Visible = false;
            sideMenuRight4Label.Visible = false;
        }

        public void PrintReceipt()
        {
            if (_atmState == ATMState.WaitingForReceiptRemove)
            {
                MessageBox.Show($"ВЫПИСКА\nСЧЁТ: {_account.Id}\nБАЛАНС: {_account.VisibleMoney}$");
            }
            else if (_atmState == ATMState.WaitingForReceiptRemoveWithdraw)
            {
                MessageBox.Show($"СНЯТИЕ НАЛИЧНЫХ\nСЧЁТ: {_account.Id}\nВЫДАНО: {_transaction.Amount}$");
            }
        }

        private void receiptPrinterImage_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.WaitingForReceiptRemove)
            {
                receiptPrinterImage.Image = global::ATM_UI.Properties.Resources.receipt_printer;

                PrintReceipt();

                _atmState = ATMState.QueryStatusView;
            }

            if (_atmState == ATMState.WaitingForReceiptRemoveWithdraw)
            {
                receiptPrinterImage.Image = global::ATM_UI.Properties.Resources.receipt_printer;

                PrintReceipt();

                EndSession();
            }
        }

        private void moneyDispencerImage_Click(object sender, EventArgs e)
        {
            if (_atmState == ATMState.WaitingForMoneyRemove)
            {
                moneyDispencerImage.Image = global::ATM_UI.Properties.Resources.money_dispencer;

                if (_printReceipt)
                {
                    _atmState = ATMState.WaitingForReceiptRemoveWithdraw;
                    receiptPrinterImage.Image = global::ATM_UI.Properties.Resources.receipt_printer_printed;
                    SoundPlayer simpleSound = new SoundPlayer(global::ATM_UI.Properties.Resources.receipt_print_sound);
                    simpleSound.Play();

                    displayRow1label.Text = "Заберите чек!";
                }
                else
                {
                    EndSession();
                }
            }
        }
    }
}
