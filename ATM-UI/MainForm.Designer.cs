namespace ATM_UI
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cardreaderImage = new System.Windows.Forms.PictureBox();
            this.receiptPrinterImage = new System.Windows.Forms.PictureBox();
            this.moneyDispencerImage = new System.Windows.Forms.PictureBox();
            this.oneBtn = new System.Windows.Forms.Button();
            this.twoBtn = new System.Windows.Forms.Button();
            this.threeBtn = new System.Windows.Forms.Button();
            this.fourBtn = new System.Windows.Forms.Button();
            this.fiveBtn = new System.Windows.Forms.Button();
            this.sixBtn = new System.Windows.Forms.Button();
            this.sevenBtn = new System.Windows.Forms.Button();
            this.eightBtn = new System.Windows.Forms.Button();
            this.nineBtn = new System.Windows.Forms.Button();
            this.zeroBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.clearBtn = new System.Windows.Forms.Button();
            this.enterBtn = new System.Windows.Forms.Button();
            this.leftSideFirstBtn = new System.Windows.Forms.Button();
            this.leftSideSecondBtn = new System.Windows.Forms.Button();
            this.leftSideThirdBtn = new System.Windows.Forms.Button();
            this.leftSideFourthBtn = new System.Windows.Forms.Button();
            this.rightSideFirstBtn = new System.Windows.Forms.Button();
            this.rightSideSecondBtn = new System.Windows.Forms.Button();
            this.rightSideThirdBtn = new System.Windows.Forms.Button();
            this.rightSideFourthBtn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.sideMenuRight4Label = new System.Windows.Forms.Label();
            this.sideMenuRight3Label = new System.Windows.Forms.Label();
            this.sideMenuRight2Label = new System.Windows.Forms.Label();
            this.sideMenuRight1Label = new System.Windows.Forms.Label();
            this.sideMenuLeft4Label = new System.Windows.Forms.Label();
            this.sideMenuLeft3Label = new System.Windows.Forms.Label();
            this.sideMenuLeft2Label = new System.Windows.Forms.Label();
            this.sideMenuLeft1Label = new System.Windows.Forms.Label();
            this.displayRow4label = new System.Windows.Forms.Label();
            this.displayRow3label = new System.Windows.Forms.Label();
            this.displayRow2label = new System.Windows.Forms.Label();
            this.displayRow1label = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardreaderImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.receiptPrinterImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyDispencerImage)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ATM_UI.Properties.Resources.pinpad_base;
            this.pictureBox1.Location = new System.Drawing.Point(12, 337);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(296, 212);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // cardreaderImage
            // 
            this.cardreaderImage.Image = global::ATM_UI.Properties.Resources.reader;
            this.cardreaderImage.Location = new System.Drawing.Point(315, 398);
            this.cardreaderImage.Name = "cardreaderImage";
            this.cardreaderImage.Size = new System.Drawing.Size(279, 151);
            this.cardreaderImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cardreaderImage.TabIndex = 1;
            this.cardreaderImage.TabStop = false;
            this.cardreaderImage.Click += new System.EventHandler(this.cardreaderImage_Click);
            // 
            // receiptPrinterImage
            // 
            this.receiptPrinterImage.Image = global::ATM_UI.Properties.Resources.receipt_printer;
            this.receiptPrinterImage.Location = new System.Drawing.Point(315, 337);
            this.receiptPrinterImage.Name = "receiptPrinterImage";
            this.receiptPrinterImage.Size = new System.Drawing.Size(279, 55);
            this.receiptPrinterImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.receiptPrinterImage.TabIndex = 2;
            this.receiptPrinterImage.TabStop = false;
            this.receiptPrinterImage.Click += new System.EventHandler(this.receiptPrinterImage_Click);
            // 
            // moneyDispencerImage
            // 
            this.moneyDispencerImage.Image = global::ATM_UI.Properties.Resources.money_dispencer;
            this.moneyDispencerImage.Location = new System.Drawing.Point(12, 555);
            this.moneyDispencerImage.Name = "moneyDispencerImage";
            this.moneyDispencerImage.Size = new System.Drawing.Size(582, 139);
            this.moneyDispencerImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.moneyDispencerImage.TabIndex = 3;
            this.moneyDispencerImage.TabStop = false;
            this.moneyDispencerImage.Click += new System.EventHandler(this.moneyDispencerImage_Click);
            // 
            // oneBtn
            // 
            this.oneBtn.Location = new System.Drawing.Point(41, 365);
            this.oneBtn.Name = "oneBtn";
            this.oneBtn.Size = new System.Drawing.Size(41, 27);
            this.oneBtn.TabIndex = 4;
            this.oneBtn.Text = "1";
            this.oneBtn.UseVisualStyleBackColor = true;
            this.oneBtn.Click += new System.EventHandler(this.oneBtn_Click);
            // 
            // twoBtn
            // 
            this.twoBtn.Location = new System.Drawing.Point(100, 365);
            this.twoBtn.Name = "twoBtn";
            this.twoBtn.Size = new System.Drawing.Size(41, 27);
            this.twoBtn.TabIndex = 5;
            this.twoBtn.Text = "2";
            this.twoBtn.UseVisualStyleBackColor = true;
            this.twoBtn.Click += new System.EventHandler(this.twoBtn_Click);
            // 
            // threeBtn
            // 
            this.threeBtn.Location = new System.Drawing.Point(159, 365);
            this.threeBtn.Name = "threeBtn";
            this.threeBtn.Size = new System.Drawing.Size(41, 27);
            this.threeBtn.TabIndex = 6;
            this.threeBtn.Text = "3";
            this.threeBtn.UseVisualStyleBackColor = true;
            this.threeBtn.Click += new System.EventHandler(this.threeBtn_Click);
            // 
            // fourBtn
            // 
            this.fourBtn.Location = new System.Drawing.Point(41, 409);
            this.fourBtn.Name = "fourBtn";
            this.fourBtn.Size = new System.Drawing.Size(41, 27);
            this.fourBtn.TabIndex = 7;
            this.fourBtn.Text = "4";
            this.fourBtn.UseVisualStyleBackColor = true;
            this.fourBtn.Click += new System.EventHandler(this.fourBtn_Click);
            // 
            // fiveBtn
            // 
            this.fiveBtn.Location = new System.Drawing.Point(100, 409);
            this.fiveBtn.Name = "fiveBtn";
            this.fiveBtn.Size = new System.Drawing.Size(41, 27);
            this.fiveBtn.TabIndex = 8;
            this.fiveBtn.Text = "5";
            this.fiveBtn.UseVisualStyleBackColor = true;
            this.fiveBtn.Click += new System.EventHandler(this.fiveBtn_Click);
            // 
            // sixBtn
            // 
            this.sixBtn.Location = new System.Drawing.Point(159, 409);
            this.sixBtn.Name = "sixBtn";
            this.sixBtn.Size = new System.Drawing.Size(41, 27);
            this.sixBtn.TabIndex = 9;
            this.sixBtn.Text = "6";
            this.sixBtn.UseVisualStyleBackColor = true;
            this.sixBtn.Click += new System.EventHandler(this.sixBtn_Click);
            // 
            // sevenBtn
            // 
            this.sevenBtn.Location = new System.Drawing.Point(41, 453);
            this.sevenBtn.Name = "sevenBtn";
            this.sevenBtn.Size = new System.Drawing.Size(41, 27);
            this.sevenBtn.TabIndex = 10;
            this.sevenBtn.Text = "7";
            this.sevenBtn.UseVisualStyleBackColor = true;
            this.sevenBtn.Click += new System.EventHandler(this.sevenBtn_Click);
            // 
            // eightBtn
            // 
            this.eightBtn.Location = new System.Drawing.Point(100, 453);
            this.eightBtn.Name = "eightBtn";
            this.eightBtn.Size = new System.Drawing.Size(41, 27);
            this.eightBtn.TabIndex = 11;
            this.eightBtn.Text = "8";
            this.eightBtn.UseVisualStyleBackColor = true;
            this.eightBtn.Click += new System.EventHandler(this.eightBtn_Click);
            // 
            // nineBtn
            // 
            this.nineBtn.Location = new System.Drawing.Point(159, 453);
            this.nineBtn.Name = "nineBtn";
            this.nineBtn.Size = new System.Drawing.Size(41, 27);
            this.nineBtn.TabIndex = 12;
            this.nineBtn.Text = "9";
            this.nineBtn.UseVisualStyleBackColor = true;
            this.nineBtn.Click += new System.EventHandler(this.nineBtn_Click);
            // 
            // zeroBtn
            // 
            this.zeroBtn.Location = new System.Drawing.Point(100, 498);
            this.zeroBtn.Name = "zeroBtn";
            this.zeroBtn.Size = new System.Drawing.Size(41, 27);
            this.zeroBtn.TabIndex = 13;
            this.zeroBtn.Text = "0";
            this.zeroBtn.UseVisualStyleBackColor = true;
            this.zeroBtn.Click += new System.EventHandler(this.zeroBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.BackColor = System.Drawing.Color.Red;
            this.cancelBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cancelBtn.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.cancelBtn.ForeColor = System.Drawing.Color.Black;
            this.cancelBtn.Location = new System.Drawing.Point(234, 365);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(49, 27);
            this.cancelBtn.TabIndex = 14;
            this.cancelBtn.Text = "CANCEL";
            this.cancelBtn.UseVisualStyleBackColor = false;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // clearBtn
            // 
            this.clearBtn.BackColor = System.Drawing.Color.Yellow;
            this.clearBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.clearBtn.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.clearBtn.Location = new System.Drawing.Point(234, 409);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(49, 27);
            this.clearBtn.TabIndex = 15;
            this.clearBtn.Text = "CLEAR";
            this.clearBtn.UseVisualStyleBackColor = false;
            this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
            // 
            // enterBtn
            // 
            this.enterBtn.BackColor = System.Drawing.Color.Green;
            this.enterBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.enterBtn.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.enterBtn.Location = new System.Drawing.Point(234, 453);
            this.enterBtn.Name = "enterBtn";
            this.enterBtn.Size = new System.Drawing.Size(49, 27);
            this.enterBtn.TabIndex = 16;
            this.enterBtn.Text = "ENTER";
            this.enterBtn.UseVisualStyleBackColor = false;
            this.enterBtn.Click += new System.EventHandler(this.enterBtn_Click);
            // 
            // leftSideFirstBtn
            // 
            this.leftSideFirstBtn.BackgroundImage = global::ATM_UI.Properties.Resources.selectionBtn_texture;
            this.leftSideFirstBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.leftSideFirstBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.leftSideFirstBtn.Location = new System.Drawing.Point(12, 36);
            this.leftSideFirstBtn.Name = "leftSideFirstBtn";
            this.leftSideFirstBtn.Size = new System.Drawing.Size(86, 56);
            this.leftSideFirstBtn.TabIndex = 17;
            this.leftSideFirstBtn.UseVisualStyleBackColor = true;
            this.leftSideFirstBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // leftSideSecondBtn
            // 
            this.leftSideSecondBtn.BackgroundImage = global::ATM_UI.Properties.Resources.selectionBtn_texture;
            this.leftSideSecondBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.leftSideSecondBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.leftSideSecondBtn.Location = new System.Drawing.Point(12, 98);
            this.leftSideSecondBtn.Name = "leftSideSecondBtn";
            this.leftSideSecondBtn.Size = new System.Drawing.Size(86, 56);
            this.leftSideSecondBtn.TabIndex = 18;
            this.leftSideSecondBtn.UseVisualStyleBackColor = true;
            this.leftSideSecondBtn.Click += new System.EventHandler(this.leftSideSecondBtn_Click);
            // 
            // leftSideThirdBtn
            // 
            this.leftSideThirdBtn.BackgroundImage = global::ATM_UI.Properties.Resources.selectionBtn_texture;
            this.leftSideThirdBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.leftSideThirdBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.leftSideThirdBtn.Location = new System.Drawing.Point(12, 160);
            this.leftSideThirdBtn.Name = "leftSideThirdBtn";
            this.leftSideThirdBtn.Size = new System.Drawing.Size(86, 56);
            this.leftSideThirdBtn.TabIndex = 19;
            this.leftSideThirdBtn.UseVisualStyleBackColor = true;
            this.leftSideThirdBtn.Click += new System.EventHandler(this.leftSideThirdBtn_Click);
            // 
            // leftSideFourthBtn
            // 
            this.leftSideFourthBtn.BackgroundImage = global::ATM_UI.Properties.Resources.selectionBtn_texture;
            this.leftSideFourthBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.leftSideFourthBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.leftSideFourthBtn.Location = new System.Drawing.Point(12, 222);
            this.leftSideFourthBtn.Name = "leftSideFourthBtn";
            this.leftSideFourthBtn.Size = new System.Drawing.Size(86, 56);
            this.leftSideFourthBtn.TabIndex = 20;
            this.leftSideFourthBtn.UseVisualStyleBackColor = true;
            this.leftSideFourthBtn.Click += new System.EventHandler(this.leftSideFourthBtn_Click);
            // 
            // rightSideFirstBtn
            // 
            this.rightSideFirstBtn.BackgroundImage = global::ATM_UI.Properties.Resources.selectionBtn_texture;
            this.rightSideFirstBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.rightSideFirstBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rightSideFirstBtn.Location = new System.Drawing.Point(508, 36);
            this.rightSideFirstBtn.Name = "rightSideFirstBtn";
            this.rightSideFirstBtn.Size = new System.Drawing.Size(86, 56);
            this.rightSideFirstBtn.TabIndex = 21;
            this.rightSideFirstBtn.UseVisualStyleBackColor = true;
            this.rightSideFirstBtn.Click += new System.EventHandler(this.rightSideFirstBtn_Click);
            // 
            // rightSideSecondBtn
            // 
            this.rightSideSecondBtn.BackgroundImage = global::ATM_UI.Properties.Resources.selectionBtn_texture;
            this.rightSideSecondBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.rightSideSecondBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rightSideSecondBtn.Location = new System.Drawing.Point(508, 98);
            this.rightSideSecondBtn.Name = "rightSideSecondBtn";
            this.rightSideSecondBtn.Size = new System.Drawing.Size(86, 56);
            this.rightSideSecondBtn.TabIndex = 22;
            this.rightSideSecondBtn.UseVisualStyleBackColor = true;
            this.rightSideSecondBtn.Click += new System.EventHandler(this.rightSideSecondBtn_Click);
            // 
            // rightSideThirdBtn
            // 
            this.rightSideThirdBtn.BackgroundImage = global::ATM_UI.Properties.Resources.selectionBtn_texture;
            this.rightSideThirdBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.rightSideThirdBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rightSideThirdBtn.Location = new System.Drawing.Point(508, 160);
            this.rightSideThirdBtn.Name = "rightSideThirdBtn";
            this.rightSideThirdBtn.Size = new System.Drawing.Size(86, 56);
            this.rightSideThirdBtn.TabIndex = 23;
            this.rightSideThirdBtn.UseVisualStyleBackColor = true;
            this.rightSideThirdBtn.Click += new System.EventHandler(this.rightSideThirdBtn_Click);
            // 
            // rightSideFourthBtn
            // 
            this.rightSideFourthBtn.BackgroundImage = global::ATM_UI.Properties.Resources.selectionBtn_texture;
            this.rightSideFourthBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.rightSideFourthBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rightSideFourthBtn.Location = new System.Drawing.Point(508, 222);
            this.rightSideFourthBtn.Name = "rightSideFourthBtn";
            this.rightSideFourthBtn.Size = new System.Drawing.Size(86, 56);
            this.rightSideFourthBtn.TabIndex = 24;
            this.rightSideFourthBtn.UseVisualStyleBackColor = true;
            this.rightSideFourthBtn.Click += new System.EventHandler(this.rightSideFourthBtn_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.sideMenuRight4Label);
            this.panel1.Controls.Add(this.sideMenuRight3Label);
            this.panel1.Controls.Add(this.sideMenuRight2Label);
            this.panel1.Controls.Add(this.sideMenuRight1Label);
            this.panel1.Controls.Add(this.sideMenuLeft4Label);
            this.panel1.Controls.Add(this.sideMenuLeft3Label);
            this.panel1.Controls.Add(this.sideMenuLeft2Label);
            this.panel1.Controls.Add(this.sideMenuLeft1Label);
            this.panel1.Controls.Add(this.displayRow4label);
            this.panel1.Controls.Add(this.displayRow3label);
            this.panel1.Controls.Add(this.displayRow2label);
            this.panel1.Controls.Add(this.displayRow1label);
            this.panel1.Location = new System.Drawing.Point(104, 36);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(398, 242);
            this.panel1.TabIndex = 25;
            // 
            // sideMenuRight4Label
            // 
            this.sideMenuRight4Label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.sideMenuRight4Label.Location = new System.Drawing.Point(199, 184);
            this.sideMenuRight4Label.Name = "sideMenuRight4Label";
            this.sideMenuRight4Label.Size = new System.Drawing.Size(196, 56);
            this.sideMenuRight4Label.TabIndex = 11;
            this.sideMenuRight4Label.Text = "Заголовок";
            this.sideMenuRight4Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.sideMenuRight4Label.Visible = false;
            // 
            // sideMenuRight3Label
            // 
            this.sideMenuRight3Label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.sideMenuRight3Label.Location = new System.Drawing.Point(199, 122);
            this.sideMenuRight3Label.Name = "sideMenuRight3Label";
            this.sideMenuRight3Label.Size = new System.Drawing.Size(196, 56);
            this.sideMenuRight3Label.TabIndex = 10;
            this.sideMenuRight3Label.Text = "Заголовок";
            this.sideMenuRight3Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.sideMenuRight3Label.Visible = false;
            // 
            // sideMenuRight2Label
            // 
            this.sideMenuRight2Label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.sideMenuRight2Label.Location = new System.Drawing.Point(199, 60);
            this.sideMenuRight2Label.Name = "sideMenuRight2Label";
            this.sideMenuRight2Label.Size = new System.Drawing.Size(196, 56);
            this.sideMenuRight2Label.TabIndex = 9;
            this.sideMenuRight2Label.Text = "Заголовок";
            this.sideMenuRight2Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.sideMenuRight2Label.Visible = false;
            // 
            // sideMenuRight1Label
            // 
            this.sideMenuRight1Label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.sideMenuRight1Label.Location = new System.Drawing.Point(199, -2);
            this.sideMenuRight1Label.Name = "sideMenuRight1Label";
            this.sideMenuRight1Label.Size = new System.Drawing.Size(196, 56);
            this.sideMenuRight1Label.TabIndex = 8;
            this.sideMenuRight1Label.Text = "Заголовок";
            this.sideMenuRight1Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.sideMenuRight1Label.Visible = false;
            // 
            // sideMenuLeft4Label
            // 
            this.sideMenuLeft4Label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.sideMenuLeft4Label.Location = new System.Drawing.Point(3, 184);
            this.sideMenuLeft4Label.Name = "sideMenuLeft4Label";
            this.sideMenuLeft4Label.Size = new System.Drawing.Size(196, 56);
            this.sideMenuLeft4Label.TabIndex = 7;
            this.sideMenuLeft4Label.Text = "Заголовок";
            this.sideMenuLeft4Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.sideMenuLeft4Label.Visible = false;
            // 
            // sideMenuLeft3Label
            // 
            this.sideMenuLeft3Label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.sideMenuLeft3Label.Location = new System.Drawing.Point(3, 122);
            this.sideMenuLeft3Label.Name = "sideMenuLeft3Label";
            this.sideMenuLeft3Label.Size = new System.Drawing.Size(196, 56);
            this.sideMenuLeft3Label.TabIndex = 6;
            this.sideMenuLeft3Label.Text = "Заголовок";
            this.sideMenuLeft3Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.sideMenuLeft3Label.Visible = false;
            // 
            // sideMenuLeft2Label
            // 
            this.sideMenuLeft2Label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.sideMenuLeft2Label.Location = new System.Drawing.Point(3, 60);
            this.sideMenuLeft2Label.Name = "sideMenuLeft2Label";
            this.sideMenuLeft2Label.Size = new System.Drawing.Size(196, 56);
            this.sideMenuLeft2Label.TabIndex = 5;
            this.sideMenuLeft2Label.Text = "Заголовок";
            this.sideMenuLeft2Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.sideMenuLeft2Label.Visible = false;
            // 
            // sideMenuLeft1Label
            // 
            this.sideMenuLeft1Label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.sideMenuLeft1Label.Location = new System.Drawing.Point(3, -2);
            this.sideMenuLeft1Label.Name = "sideMenuLeft1Label";
            this.sideMenuLeft1Label.Size = new System.Drawing.Size(196, 56);
            this.sideMenuLeft1Label.TabIndex = 4;
            this.sideMenuLeft1Label.Text = "Заголовок";
            this.sideMenuLeft1Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.sideMenuLeft1Label.Visible = false;
            // 
            // displayRow4label
            // 
            this.displayRow4label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.displayRow4label.Location = new System.Drawing.Point(3, 186);
            this.displayRow4label.Name = "displayRow4label";
            this.displayRow4label.Size = new System.Drawing.Size(392, 56);
            this.displayRow4label.TabIndex = 3;
            this.displayRow4label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // displayRow3label
            // 
            this.displayRow3label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.displayRow3label.Location = new System.Drawing.Point(3, 124);
            this.displayRow3label.Name = "displayRow3label";
            this.displayRow3label.Size = new System.Drawing.Size(392, 56);
            this.displayRow3label.TabIndex = 2;
            this.displayRow3label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // displayRow2label
            // 
            this.displayRow2label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.displayRow2label.Location = new System.Drawing.Point(3, 62);
            this.displayRow2label.Name = "displayRow2label";
            this.displayRow2label.Size = new System.Drawing.Size(392, 56);
            this.displayRow2label.TabIndex = 1;
            this.displayRow2label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // displayRow1label
            // 
            this.displayRow1label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.displayRow1label.Location = new System.Drawing.Point(3, 0);
            this.displayRow1label.Name = "displayRow1label";
            this.displayRow1label.Size = new System.Drawing.Size(392, 56);
            this.displayRow1label.TabIndex = 0;
            this.displayRow1label.Text = "Вставьте карту";
            this.displayRow1label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.MidnightBlue;
            this.ClientSize = new System.Drawing.Size(606, 706);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.rightSideFourthBtn);
            this.Controls.Add(this.rightSideThirdBtn);
            this.Controls.Add(this.rightSideSecondBtn);
            this.Controls.Add(this.rightSideFirstBtn);
            this.Controls.Add(this.leftSideFourthBtn);
            this.Controls.Add(this.leftSideThirdBtn);
            this.Controls.Add(this.leftSideSecondBtn);
            this.Controls.Add(this.leftSideFirstBtn);
            this.Controls.Add(this.enterBtn);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.zeroBtn);
            this.Controls.Add(this.nineBtn);
            this.Controls.Add(this.eightBtn);
            this.Controls.Add(this.sevenBtn);
            this.Controls.Add(this.sixBtn);
            this.Controls.Add(this.fiveBtn);
            this.Controls.Add(this.fourBtn);
            this.Controls.Add(this.threeBtn);
            this.Controls.Add(this.twoBtn);
            this.Controls.Add(this.oneBtn);
            this.Controls.Add(this.moneyDispencerImage);
            this.Controls.Add(this.receiptPrinterImage);
            this.Controls.Add(this.cardreaderImage);
            this.Controls.Add(this.pictureBox1);
            this.MaximizeBox = false;
            this.MdiChildrenMinimizedAnchorBottom = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Симулятор \"Банкомат\"";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardreaderImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.receiptPrinterImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyDispencerImage)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private PictureBox pictureBox1;
        private PictureBox cardreaderImage;
        private PictureBox receiptPrinterImage;
        private PictureBox moneyDispencerImage;
        private Button oneBtn;
        private Button twoBtn;
        private Button threeBtn;
        private Button fourBtn;
        private Button fiveBtn;
        private Button sixBtn;
        private Button sevenBtn;
        private Button eightBtn;
        private Button nineBtn;
        private Button zeroBtn;
        private Button cancelBtn;
        private Button clearBtn;
        private Button enterBtn;
        private Button leftSideFirstBtn;
        private Button leftSideSecondBtn;
        private Button leftSideThirdBtn;
        private Button leftSideFourthBtn;
        private Button rightSideFirstBtn;
        private Button rightSideSecondBtn;
        private Button rightSideThirdBtn;
        private Button rightSideFourthBtn;
        private Panel panel1;
        private Label displayRow4label;
        private Label displayRow3label;
        private Label displayRow2label;
        private Label displayRow1label;
        private System.Windows.Forms.Timer timer1;
        private Label sideMenuLeft4Label;
        private Label sideMenuLeft3Label;
        private Label sideMenuLeft2Label;
        private Label sideMenuLeft1Label;
        private Label sideMenuRight4Label;
        private Label sideMenuRight3Label;
        private Label sideMenuRight2Label;
        private Label sideMenuRight1Label;
    }
}
