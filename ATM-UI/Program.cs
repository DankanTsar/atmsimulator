using ATM.Model;

namespace ATM_UI
{
    internal static class Program
    {
        private static HttpClient _httpClient = new HttpClient();
        internal static ATM.Proxy.AtmProxyLayer? Proxy;
        internal static ATM.Model.ATM? ATM;

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // To customize application configuration such as set high DPI settings or default font,
            // see https://aka.ms/applicationconfiguration.
            ApplicationConfiguration.Initialize();
            //Application.Run(new LoginForm());

            //DebugInit();

            Application.Run(new LoginForm());
            if (ATM is not null)
                Application.Run(new MainForm());
        }

        private static void DebugInit()
        {
            var confiscatedcrate = new ConfiscatedCrate();
            var moneycrate = new MoneyCrate(10, 5, 15, 12, 17, 16, 1);
            var atm = new ATM.Model.ATM(
                "12345", "eef1394ee3b80d9ce18ebfe618a056e6e0299c12cd19c99c796d482eb52277c6", "54321", moneycrate,
                confiscatedcrate);

            var account = new Account("Ivanov", "Ivan", 35000);
            var card = new Card("4929515245979072", "7227002b2881e63af250c54631eb42c0736f4b2c2deb1911846cb659d4a3c23c",
                "787878");
            account.Cards.Add(card);


            using (var context = new EFAppContext(drop: true))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                context.ATMs.Add(atm);
                context.Cards.Add(card);
                context.Accounts.Add(account);

                context.SaveChanges();
            }

            ATM = atm;
        }
    }
}
