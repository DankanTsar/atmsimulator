﻿using ATM.Proxy;

namespace ATM_UI
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void loginBtn_Click(object sender, EventArgs e)
        {
            Program.Proxy = new AtmProxyLayer($"{apiUrlTextBox.Text}");
            var curAtm = Program.Proxy.Authenticate(loginTextBox.Text, passwordTextBox.Text);
            if (curAtm is not null)
            {
                Program.ATM = curAtm;
                this.Close();
            }
            else
            {
                throw new UnauthorizedAccessException();
            }
        }
    }
}
