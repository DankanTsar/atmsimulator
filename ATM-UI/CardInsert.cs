﻿namespace ATM_UI
{
    public partial class CardInsert : Form
    {
        public string cardPan;

        public CardInsert()
        {
            InitializeComponent();
        }

        private void insertCardBtn_Click(object sender, EventArgs e)
        {
            cardPan = cardPanTextBox.Text;

            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
