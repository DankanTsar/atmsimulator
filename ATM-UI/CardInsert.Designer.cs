﻿namespace ATM_UI
{
    partial class CardInsert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cardPanLabel = new System.Windows.Forms.Label();
            this.cardPanTextBox = new System.Windows.Forms.TextBox();
            this.insertCardBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cardPanLabel
            // 
            this.cardPanLabel.AutoSize = true;
            this.cardPanLabel.Location = new System.Drawing.Point(12, 15);
            this.cardPanLabel.Name = "cardPanLabel";
            this.cardPanLabel.Size = new System.Drawing.Size(67, 15);
            this.cardPanLabel.TabIndex = 0;
            this.cardPanLabel.Text = "PAN Карты";
            // 
            // cardPanTextBox
            // 
            this.cardPanTextBox.Location = new System.Drawing.Point(85, 12);
            this.cardPanTextBox.Name = "cardPanTextBox";
            this.cardPanTextBox.Size = new System.Drawing.Size(255, 23);
            this.cardPanTextBox.TabIndex = 1;
            // 
            // insertCardBtn
            // 
            this.insertCardBtn.Location = new System.Drawing.Point(254, 41);
            this.insertCardBtn.Name = "insertCardBtn";
            this.insertCardBtn.Size = new System.Drawing.Size(86, 23);
            this.insertCardBtn.TabIndex = 2;
            this.insertCardBtn.Text = "Вставить";
            this.insertCardBtn.UseVisualStyleBackColor = true;
            this.insertCardBtn.Click += new System.EventHandler(this.insertCardBtn_Click);
            // 
            // CardInsert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(352, 70);
            this.Controls.Add(this.insertCardBtn);
            this.Controls.Add(this.cardPanTextBox);
            this.Controls.Add(this.cardPanLabel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CardInsert";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Ридер карты";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label cardPanLabel;
        private TextBox cardPanTextBox;
        private Button insertCardBtn;
    }
}