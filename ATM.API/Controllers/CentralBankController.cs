using System.Security.Cryptography;
using System.Text;
using ATM.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ATM.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CentralBankController : ControllerBase
    {
        private readonly ILogger<CentralBankController> _logger;

        public CentralBankController(ILogger<CentralBankController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Метод аутентификации самого банкомата при запуске
        /// </summary>
        /// <param name="login">Уникальный логин банкомата</param>
        /// <param name="password">Пароль банкомата</param>
        /// <returns>Инстанс типа "Банкомат"</returns>
        /// <exception cref="UnauthorizedAccessException"> При вводе некорректных данных лучше упасть. </exception>
        [HttpGet]
        [Route("AuthenticateATM/{login}/{password}")]
        public Model.ATM Authenticate(string login, string password)
        {
            using (var context = new ATM.Model.EFAppContext())
            {
                var atm = context.ATMs.Include(atm1 => atm1.MoneyCrate)
                    .Include(atm2 => atm2.ConfiscatedCrate)
                    .FirstOrDefault(a => a.Login == login);

                if (atm?.Login != login)
                    throw new UnauthorizedAccessException();


                #region Валидация пароля

                using (var hasher = SHA256.Create())
                {
                    hasher.ComputeHash(Encoding.UTF8.GetBytes(password + atm.Salt));
                    var hashstring = new StringBuilder();

                    foreach (byte b in hasher.Hash)
                        hashstring.Append(b.ToString("x2"));
                    hasher.Clear();

                    if (hashstring.ToString() == atm.Password)
                    {
                        hashstring.Clear();
                        return atm;
                    }

                    hashstring.Clear();
                }

                throw new UnauthorizedAccessException();

                #endregion
            }
        }

        /// <summary>
        /// Метод возвращающий все данные о карте по его PAN
        /// </summary>
        /// <param name="pan">PAN карты</param>
        /// <returns>Инстанс типа Card содержащий данные запрошенной карты</returns>
        [HttpGet]
        [Route("GetCard/{pan}")]
        public Card GetCard(string pan)
        {
            using (var context = new ATM.Model.EFAppContext())
            {
                var card = context.Cards.FirstOrDefault(c => c.Pan == pan);
                return card;
            }
        }

        /// <summary>
        /// Метод проверки/авторизации карты
        /// </summary>
        /// <param name="pan">PAN карты</param>
        /// <param name="pin">PIN, введённый пользователем</param>
        /// <returns>Результат проверки кода карты. true если код верный, false если неверный</returns>
        /// <exception cref="UnauthorizedAccessException"> Теоретически заблокированная карта сразу конфискуется, соответственно если была вставлена конфискованная карта это -- что-то очень неправильное</exception>
        [HttpGet]
        [Route("AuthenticateCard")]
        public bool AuthenticateCard(string pan, string pin)
        {
            using (var context = new ATM.Model.EFAppContext())
            {
                var card = context.Cards.FirstOrDefault(c => c.Pan == pan);

                if (card?.Pan != pan)
                    return false;

                if (card.CardStatus == CardStatus.PINLocked)
                    throw new UnauthorizedAccessException();

                #region Валидация PIN

                using (var hasher = SHA256.Create())
                {
                    hasher.ComputeHash(Encoding.UTF8.GetBytes(pin + card.Salt));
                    var hashstring = new StringBuilder();

                    foreach (byte b in hasher.Hash)
                        hashstring.Append(b.ToString("x2"));
                    hasher.Clear();

                    if (hashstring.ToString() == card.Pin)
                    {
                        hashstring.Clear();
                        card.PinAttempts =
                            3; // Сбрасываем попытки входа к начальному значению, когда введён корректный пароль
                        context.SaveChanges();

                        //var account = context.Accounts.FirstOrDefault(acc => acc.Cards.Contains(card));

                        //if (account is not default(Account))
                        //    return account;
                        return true;
                    }

                    card.PinAttempts--;
                    card.CardStatus = card.CardStatus;
                    context.SaveChanges();
                    hashstring.Clear();
                }

                return false;

                #endregion
            }
        }

        /// <summary>
        /// Метод для проведения действий для "конфискации" карты
        /// </summary>
        /// <param name="pan">PAN карты</param>
        /// <param name="atm">Id банкомата</param>
        [HttpGet]
        [Route("ConfiscateCard/{pan}/{atm}")]
        public void ConfiscateCard(string pan, int atm)
        {
            using (var context = new ATM.Model.EFAppContext())
            {
                var atmInstance = context.ATMs.FirstOrDefault(a => a.Id == atm);
                var card = context.Cards.FirstOrDefault(c => c.Pan == pan);

                if (atmInstance is null)
                    return;
                if (card is null)
                    return;

                atmInstance.ConfiscatedCrate.ConfiscatedCards.Add(card);

                context.Update(atmInstance);
                context.Update(card);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Метод для создания транзакции.
        /// Осуществляет проверку наличия Д/С на счёте клиента, проверяет, что банкомат имеет возможность выдать
        /// запрошенную сумму. Записывает подробную информацию о транзакции в БД.
        /// </summary>
        /// <param name="atmId">Id банкомата</param>
        /// <param name="pan">PAN карты</param>
        /// <param name="amount">Запрашиваемая сумма в USD (банкомат оперирует только основными номиналами и
        /// ничего не знает о существовании дробных/разменных единицах)</param>
        /// <param name="receipt">Печатать ли чек</param>
        /// <returns>Инстанс транзакции</returns>
        /// <exception cref="Exception">Падаем, если не хватает аргументов.</exception>
        [HttpGet]
        [Route("WithdrawMoney/{atmId:int}/{pan}/{amount:int}/{receipt:bool}")]
        public Transaction WithdrawMoney(int atmId, string pan, int amount, bool receipt)
        {
            using (var context = new EFAppContext())
            {
                var atm = context.ATMs.Include(atm1 => atm1.MoneyCrate).FirstOrDefault(atm => atm.Id == atmId);
                var account = context.Accounts.FirstOrDefault(a =>
                    a.Cards.Contains(context.Cards.FirstOrDefault(card => card.Pan == pan)));

                if (atm == null || account == null)
                {
                    throw new Exception();
                }

                Transaction transaction;

                if (account.Money / 100 < amount)
                {
                    transaction = new Transaction(amount, account, DateTime.Now, receipt,
                        TransactionStatus.InsufficientAccountFunds);

                    context.Transactions.Add(transaction);
                    context.SaveChanges();

                    return transaction;
                }

                if (atm.MoneyCrate.Amount < amount)
                {
                    transaction = new Transaction(amount, account, DateTime.Now, receipt,
                        TransactionStatus.InsufficientAtmMoney);

                    context.Transactions.Add(transaction);
                    context.SaveChanges();

                    return transaction;
                }

                #region сборка купюр для выдачи

                int[] crateState = atm.MoneyCrate.CrateState.Reverse().ToArray();
                int[] withdrawState = { 0, 0, 0, 0, 0, 0, 0 };
                int[] registerBase = { 100, 50, 20, 10, 5, 2, 1 };

                var reg = amount;

                for (var i = 0; i < registerBase.Length; i++)
                {
                    withdrawState[i] = reg / registerBase[i];
                    if (withdrawState[i] > crateState[i])
                        withdrawState[i] -= withdrawState[i] - crateState[i];
                    reg -= withdrawState[i] * registerBase[i];
                }

                if (reg > 0)
                {
                    transaction = new Transaction(amount, account, DateTime.Now, receipt,
                        TransactionStatus.InsufficientAtmMoney);

                    context.Transactions.Add(transaction);
                    context.SaveChanges();

                    return transaction;
                }

                #endregion

                atm.MoneyCrate.UpdateState(withdrawState);
                account.Money -= amount * 100;

                transaction = new Transaction(amount, account, DateTime.Now, receipt,
                    TransactionStatus.Completed);

                context.ATMs.Update(atm);
                context.Transactions.Add(transaction);
                context.SaveChanges();

                return transaction;
            }
        }

        /// <summary>
        /// Метод для получения аккаунта по PAN привязаной карты
        /// </summary>
        /// <param name="pan">PAN карты</param>
        /// <returns>Инстанс банковского аккаунта</returns>
        [HttpGet]
        [Route("GetAccount/{pan}")]
        public Account GetAccount(string pan)
        {
            using (var context = new EFAppContext())
            {
                return context.Accounts.Include(acc => acc.Cards).FirstOrDefault(acc =>
                    acc.Cards.Contains(context.Cards.FirstOrDefault(card => card.Pan == pan)));
            }
        }
    }
}
