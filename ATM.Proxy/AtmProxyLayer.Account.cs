﻿using ATM.Model;
using Newtonsoft.Json;

namespace ATM.Proxy
{
    partial class AtmProxyLayer
    {
        public Account GetAccount(string pan)
        {
            var result = Task.Run(() => _HttpClient.GetStringAsync($"/CentralBank/GetAccount/{pan}")).Result;
            return JsonConvert.DeserializeObject<Account>(result);
        }
    }
}
