﻿using Newtonsoft.Json;

namespace ATM.Proxy
{
    partial class AtmProxyLayer
    {
        public Model.ATM? Authenticate(string login, string password)
        {
            Model.ATM? atm = null;
            // Добавить обработку неправильных паролей
            var result = Task.Run(() => _HttpClient.GetStringAsync($"/CentralBank/AuthenticateATM/{login}/{password}"))
                .Result;
            atm = JsonConvert.DeserializeObject<Model.ATM>(result);
            return atm;
        }
    }
}
