using ATM.Model;
using Newtonsoft.Json;

namespace ATM.Proxy
{
    partial class AtmProxyLayer
    {
        public Transaction WithdrawMoney(int atmId, string pan, int amount, bool receipt)
        {
            // Добавить обработку неправильных паролей
            var result = Task.Run(() =>
                    _HttpClient.GetStringAsync($"/CentralBank/WithdrawMoney/{atmId}/{pan}/{amount}/{receipt}"))
                .Result;
            var transaction = JsonConvert.DeserializeObject<Transaction>(result);
            return transaction;
        }
    }
}
