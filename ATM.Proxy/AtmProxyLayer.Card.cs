﻿using ATM.Model;
using Newtonsoft.Json;

namespace ATM.Proxy
{
    partial class AtmProxyLayer
    {
        public Card GetCard(string pan)
        {
            Card card = null;
            var result = Task.Run(() => _HttpClient.GetStringAsync($"/CentralBank/GetCard/{pan}")).Result;
            return JsonConvert.DeserializeObject<Card>(result);
        }

        public bool AuthorizeCard(Card card, string enteredPin)
        {
            var result = Task.Run(() =>
                _HttpClient.GetStringAsync($"/CentralBank/AuthenticateCard?pan={card.Pan}&pin={enteredPin}")).Result;
            return bool.Parse(result);
        }
    }
}
