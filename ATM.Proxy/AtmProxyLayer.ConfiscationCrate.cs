namespace ATM.Proxy
{
    partial class AtmProxyLayer
    {
        public void ConfiscateCard(string pan, int atm)
        {
            Task.Run(() => _HttpClient.GetStringAsync($"/CentralBank/ConfiscateCard/{pan}/{atm}")).Wait();
        }
    }
}
