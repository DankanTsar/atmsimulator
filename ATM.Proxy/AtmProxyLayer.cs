﻿namespace ATM.Proxy
{
    public partial class AtmProxyLayer
    {
        public HttpClient _HttpClient = new HttpClient();

        public AtmProxyLayer(string apiUrl)
        {
            _HttpClient.BaseAddress = new Uri(apiUrl);
        }
    }
}
