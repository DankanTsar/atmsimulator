﻿namespace ATM.Model;

public class ConfiscatedCrate
{
    public ConfiscatedCrate()
    {
    }

    /// <summary>
    /// Идентификатор ящика с конфискованными картами
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Список конфискованных карт
    /// </summary>
    public List<Card> ConfiscatedCards { get; set; } = new List<Card>();
}
