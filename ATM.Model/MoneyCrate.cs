﻿namespace ATM.Model;

public class MoneyCrate
{
    public MoneyCrate(int oneUsd, int twoUsd, int fiveUsd, int tenUsd, int twentyUsd, int fiftyUsd, int hundredUsd)
    {
        OneUsd = oneUsd;
        TwoUsd = twoUsd;
        FiveUsd = fiveUsd;
        TenUsd = tenUsd;
        TwentyUsd = twentyUsd;
        FiftyUsd = fiftyUsd;
        HundredUsd = hundredUsd;
    }

    public MoneyCrate()
    {
    }

    /// <summary>
    /// Идентификатор ящика с деньгами
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Количество купюр достоинством 1 USD
    /// </summary>
    public int OneUsd { get; set; }

    /// <summary>
    /// Количество купюр достоинством 2 USD
    /// </summary>
    public int TwoUsd { get; set; }

    /// <summary>
    /// Количество купюр достоинством 5 USD
    /// </summary>
    public int FiveUsd { get; set; }

    /// <summary>
    /// Количество купюр достоинством 10 USD
    /// </summary>
    public int TenUsd { get; set; }

    /// <summary>
    /// Количество купюр достоинством 20 USD
    /// </summary>
    public int TwentyUsd { get; set; }

    /// <summary>
    /// Количество купюр достоинством 50 USD
    /// </summary>
    public int FiftyUsd { get; set; }

    /// <summary>
    /// Количество купюр достоинством 100 USD
    /// </summary>
    public int HundredUsd { get; set; }

    public int Amount =>
        OneUsd + TwoUsd * 2 + FiveUsd * 5 + TenUsd * 10 +
        TwentyUsd * 20 + FiftyUsd * 50 + HundredUsd * 100;

    public int[] CrateState
    {
        get { return new[] { OneUsd, TwoUsd, FiveUsd, TenUsd, TwentyUsd, FiftyUsd, HundredUsd }; }
    }

    public void UpdateState(int[] withdrawState)
    {
        HundredUsd -= withdrawState[0];
        FiftyUsd -= withdrawState[1];
        TwentyUsd -= withdrawState[2];
        TenUsd -= withdrawState[3];
        FiveUsd -= withdrawState[4];
        TwoUsd -= withdrawState[5];
        OneUsd -= withdrawState[6];
    }
}
