namespace ATM.Model;

public enum TransactionStatus
{
    Completed,
    InsufficientAccountFunds,
    InsufficientAtmMoney
}

public class Transaction
{
    public Transaction(int amount, Account transactionOwner, DateTime transactionTime, bool printReceipt,
        TransactionStatus status)
    {
        Amount = amount;
        TransactionOwner = transactionOwner;
        TransactionTime = transactionTime;
        PrintReceipt = printReceipt;
        Status = status;
    }

    public Transaction()
    {
    }

    public int Id { get; set; }

    public int Amount { get; set; }

    public Account TransactionOwner { get; set; } = new Account();

    public DateTime TransactionTime { get; set; } = DateTime.Now;

    public bool PrintReceipt { get; set; } = false;

    public TransactionStatus Status { get; set; }
}
