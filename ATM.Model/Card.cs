﻿namespace ATM.Model
{
    // Тут ещё можно напридумывать статусов для карты
    public enum CardStatus
    {
        Unlocked,
        PINLocked
    }

    public class Card
    {
        private CardStatus _cardStatus = CardStatus.Unlocked;

        public Card(string pan, string pin, string salt)
        {
            Pan = pan;
            Pin = pin;
            Salt = salt;
        }

        public Card()
        {
        }

        /// <summary>
        /// Идентификтор карты в БД
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// PAN карты (8-19 символов)
        /// </summary>
        public string Pan { get; set; } = string.Empty;

        /// <summary>
        /// Солёный хеш пин-кода карты
        /// </summary>
        public string Pin { get; set; } = string.Empty;

        /// <summary>
        /// Соль
        /// </summary>
        public string Salt { get; set; } = string.Empty;

        /// <summary>
        /// Количество доступных попыток ввода PIN
        /// </summary>
        public short PinAttempts { get; set; } = 3;

        /// <summary>
        /// Статус карты
        /// </summary>
        public CardStatus CardStatus
        {
            get => PinAttempts > 0 ? CardStatus.Unlocked : CardStatus.PINLocked;
            set => _cardStatus = value;
        }
    }
}
