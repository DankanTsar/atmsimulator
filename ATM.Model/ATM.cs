﻿namespace ATM.Model;

public class ATM
{
    public ATM(string login, string password, string salt, MoneyCrate moneyCrate, ConfiscatedCrate confiscatedCrate)
    {
        Login = login;
        Password = password;
        Salt = salt;
        MoneyCrate = moneyCrate;
        ConfiscatedCrate = confiscatedCrate;
    }

    public ATM()
    {
    }

    /// <summary>
    /// Идентификатор банкомата в БД
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Логин
    /// </summary>
    public string Login { get; set; } = string.Empty;

    /// <summary>
    /// Солёный хеш пароля банкомата
    /// </summary>
    public string Password { get; set; } = string.Empty;

    /// <summary>
    /// Соль
    /// </summary>
    public string Salt { get; set; } = string.Empty;

    public virtual MoneyCrate MoneyCrate { get; set; } = new MoneyCrate();

    public ConfiscatedCrate ConfiscatedCrate { get; set; } = new ConfiscatedCrate();
}
