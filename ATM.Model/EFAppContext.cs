using Microsoft.EntityFrameworkCore;

namespace ATM.Model;

public class EFAppContext : DbContext
{
    public DbSet<ATM> ATMs { get; set; }
    public DbSet<Card> Cards { get; set; }
    public DbSet<Account> Accounts { get; set; }
    public DbSet<Transaction> Transactions { get; set; }

    public EFAppContext()
    {
        Database.EnsureCreated();
    }

    public EFAppContext(bool drop)
    {
        if (drop == true)
            Database.EnsureDeleted();
        Database.EnsureCreated();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Filename=O:/atm.sqlite");
    }
}
