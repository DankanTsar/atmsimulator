﻿namespace ATM.Model;

public class Account
{
    public Account(string name, string surname, int money)
    {
        Name = name;
        Surname = surname;
        Money = money;
    }

    public Account()
    {
    }

    /// <summary>
    /// Идентификатор банковского аккаунта
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Имя владельца
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// Фамилия владельца
    /// </summary>
    public string? Surname { get; set; }

    /// <summary>
    /// Список карт, привязанных к счёту
    /// </summary>
    public List<Card> Cards { get; set; } = new List<Card>();

    /// <summary>
    /// Количество центов, хранимое на банковском счёте(никогда не храните деньги как плавающее!)
    /// </summary>
    public int Money { get; set; } = 0;

    /// <summary>
    /// Количество денег для отображения в интерфейсах(а тут можно, это чисто визуально)
    /// </summary>
    public decimal VisibleMoney => decimal.Round(Money / 100, 2);
}
